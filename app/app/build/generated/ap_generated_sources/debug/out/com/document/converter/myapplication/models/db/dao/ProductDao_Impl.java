package com.document.converter.myapplication.models.db.dao;

import android.database.Cursor;
import androidx.room.EntityDeletionOrUpdateAdapter;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.document.converter.myapplication.models.db.entities.ProductEntity;
import java.lang.Class;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings({"unchecked", "deprecation"})
public final class ProductDao_Impl implements ProductDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<ProductEntity> __insertionAdapterOfProductEntity;

  private final EntityDeletionOrUpdateAdapter<ProductEntity> __deletionAdapterOfProductEntity;

  private final EntityDeletionOrUpdateAdapter<ProductEntity> __updateAdapterOfProductEntity;

  public ProductDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfProductEntity = new EntityInsertionAdapter<ProductEntity>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR ABORT INTO `products` (`id`,`code`,`name`,`price`,`count`,`unit`) VALUES (nullif(?, 0),?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, ProductEntity value) {
        stmt.bindLong(1, value.id);
        if (value.code == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.code);
        }
        if (value.name == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.name);
        }
        if (value.price == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.price);
        }
        if (value.count == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.count);
        }
        if (value.unit == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.unit);
        }
      }
    };
    this.__deletionAdapterOfProductEntity = new EntityDeletionOrUpdateAdapter<ProductEntity>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `products` WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, ProductEntity value) {
        stmt.bindLong(1, value.id);
      }
    };
    this.__updateAdapterOfProductEntity = new EntityDeletionOrUpdateAdapter<ProductEntity>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR ABORT `products` SET `id` = ?,`code` = ?,`name` = ?,`price` = ?,`count` = ?,`unit` = ? WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, ProductEntity value) {
        stmt.bindLong(1, value.id);
        if (value.code == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.code);
        }
        if (value.name == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.name);
        }
        if (value.price == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.price);
        }
        if (value.count == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.count);
        }
        if (value.unit == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.unit);
        }
        stmt.bindLong(7, value.id);
      }
    };
  }

  @Override
  public void insert(final ProductEntity product) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfProductEntity.insert(product);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void delete(final ProductEntity product) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __deletionAdapterOfProductEntity.handle(product);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void update(final ProductEntity product) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __updateAdapterOfProductEntity.handle(product);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public List<ProductEntity> getAll() {
    final String _sql = "SELECT * FROM products";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfCode = CursorUtil.getColumnIndexOrThrow(_cursor, "code");
      final int _cursorIndexOfName = CursorUtil.getColumnIndexOrThrow(_cursor, "name");
      final int _cursorIndexOfPrice = CursorUtil.getColumnIndexOrThrow(_cursor, "price");
      final int _cursorIndexOfCount = CursorUtil.getColumnIndexOrThrow(_cursor, "count");
      final int _cursorIndexOfUnit = CursorUtil.getColumnIndexOrThrow(_cursor, "unit");
      final List<ProductEntity> _result = new ArrayList<ProductEntity>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final ProductEntity _item;
        _item = new ProductEntity();
        _item.id = _cursor.getLong(_cursorIndexOfId);
        if (_cursor.isNull(_cursorIndexOfCode)) {
          _item.code = null;
        } else {
          _item.code = _cursor.getString(_cursorIndexOfCode);
        }
        if (_cursor.isNull(_cursorIndexOfName)) {
          _item.name = null;
        } else {
          _item.name = _cursor.getString(_cursorIndexOfName);
        }
        if (_cursor.isNull(_cursorIndexOfPrice)) {
          _item.price = null;
        } else {
          _item.price = _cursor.getString(_cursorIndexOfPrice);
        }
        if (_cursor.isNull(_cursorIndexOfCount)) {
          _item.count = null;
        } else {
          _item.count = _cursor.getString(_cursorIndexOfCount);
        }
        if (_cursor.isNull(_cursorIndexOfUnit)) {
          _item.unit = null;
        } else {
          _item.unit = _cursor.getString(_cursorIndexOfUnit);
        }
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public ProductEntity getById(final long id) {
    final String _sql = "SELECT * FROM products WHERE id = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, id);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfCode = CursorUtil.getColumnIndexOrThrow(_cursor, "code");
      final int _cursorIndexOfName = CursorUtil.getColumnIndexOrThrow(_cursor, "name");
      final int _cursorIndexOfPrice = CursorUtil.getColumnIndexOrThrow(_cursor, "price");
      final int _cursorIndexOfCount = CursorUtil.getColumnIndexOrThrow(_cursor, "count");
      final int _cursorIndexOfUnit = CursorUtil.getColumnIndexOrThrow(_cursor, "unit");
      final ProductEntity _result;
      if(_cursor.moveToFirst()) {
        _result = new ProductEntity();
        _result.id = _cursor.getLong(_cursorIndexOfId);
        if (_cursor.isNull(_cursorIndexOfCode)) {
          _result.code = null;
        } else {
          _result.code = _cursor.getString(_cursorIndexOfCode);
        }
        if (_cursor.isNull(_cursorIndexOfName)) {
          _result.name = null;
        } else {
          _result.name = _cursor.getString(_cursorIndexOfName);
        }
        if (_cursor.isNull(_cursorIndexOfPrice)) {
          _result.price = null;
        } else {
          _result.price = _cursor.getString(_cursorIndexOfPrice);
        }
        if (_cursor.isNull(_cursorIndexOfCount)) {
          _result.count = null;
        } else {
          _result.count = _cursor.getString(_cursorIndexOfCount);
        }
        if (_cursor.isNull(_cursorIndexOfUnit)) {
          _result.unit = null;
        } else {
          _result.unit = _cursor.getString(_cursorIndexOfUnit);
        }
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  public static List<Class<?>> getRequiredConverters() {
    return Collections.emptyList();
  }
}
