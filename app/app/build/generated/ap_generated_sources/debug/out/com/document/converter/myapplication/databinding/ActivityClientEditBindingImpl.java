package com.document.converter.myapplication.databinding;
import com.document.converter.myapplication.R;
import com.document.converter.myapplication.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityClientEditBindingImpl extends ActivityClientEditBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.text_client_create_title, 7);
        sViewsWithIds.put(R.id.linear_client_create_area, 8);
        sViewsWithIds.put(R.id.text_client_edit_first_name, 9);
        sViewsWithIds.put(R.id.text_client_edit_last_name, 10);
        sViewsWithIds.put(R.id.text_client_edit_address, 11);
        sViewsWithIds.put(R.id.text_client_edit_phone, 12);
        sViewsWithIds.put(R.id.text_client_edit_code, 13);
        sViewsWithIds.put(R.id.text_client_edit_debt, 14);
        sViewsWithIds.put(R.id.button_client_edit_apply, 15);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityClientEditBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 16, sIncludes, sViewsWithIds));
    }
    private ActivityClientEditBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.Button) bindings[15]
            , (android.widget.EditText) bindings[3]
            , (android.widget.EditText) bindings[5]
            , (android.widget.EditText) bindings[6]
            , (android.widget.EditText) bindings[1]
            , (android.widget.EditText) bindings[2]
            , (android.widget.EditText) bindings[4]
            , (android.widget.LinearLayout) bindings[8]
            , (android.widget.TextView) bindings[7]
            , (android.widget.TextView) bindings[11]
            , (android.widget.TextView) bindings[13]
            , (android.widget.TextView) bindings[14]
            , (android.widget.TextView) bindings[9]
            , (android.widget.TextView) bindings[10]
            , (android.widget.TextView) bindings[12]
            );
        this.editClientAddress.setTag(null);
        this.editClientCode.setTag(null);
        this.editClientDebt.setTag(null);
        this.editClientFirstName.setTag(null);
        this.editClientLastName.setTag(null);
        this.editClientPhone.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.client == variableId) {
            setClient((com.document.converter.myapplication.models.db.entities.ClientEntity) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setClient(@Nullable com.document.converter.myapplication.models.db.entities.ClientEntity Client) {
        this.mClient = Client;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.client);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String clientPhone = null;
        java.lang.String clientDebt = null;
        com.document.converter.myapplication.models.db.entities.ClientEntity client = mClient;
        java.lang.String clientLastName = null;
        java.lang.String clientAddress = null;
        java.lang.String clientFirstName = null;
        java.lang.String clientCode = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (client != null) {
                    // read client.phone
                    clientPhone = client.phone;
                    // read client.debt
                    clientDebt = client.debt;
                    // read client.lastName
                    clientLastName = client.lastName;
                    // read client.address
                    clientAddress = client.address;
                    // read client.firstName
                    clientFirstName = client.firstName;
                    // read client.code
                    clientCode = client.code;
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.editClientAddress, clientAddress);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.editClientCode, clientCode);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.editClientDebt, clientDebt);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.editClientFirstName, clientFirstName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.editClientLastName, clientLastName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.editClientPhone, clientPhone);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): client
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}