package com.document.converter.myapplication.models.db.dao;

import android.database.Cursor;
import androidx.room.EntityDeletionOrUpdateAdapter;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.document.converter.myapplication.models.db.entities.ClientEntity;
import java.lang.Class;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings({"unchecked", "deprecation"})
public final class ClientDao_Impl implements ClientDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<ClientEntity> __insertionAdapterOfClientEntity;

  private final EntityDeletionOrUpdateAdapter<ClientEntity> __deletionAdapterOfClientEntity;

  private final EntityDeletionOrUpdateAdapter<ClientEntity> __updateAdapterOfClientEntity;

  public ClientDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfClientEntity = new EntityInsertionAdapter<ClientEntity>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR ABORT INTO `clients` (`id`,`code`,`first_name`,`last_name`,`address`,`phone`,`debt`) VALUES (nullif(?, 0),?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, ClientEntity value) {
        stmt.bindLong(1, value.id);
        if (value.code == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.code);
        }
        if (value.firstName == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.firstName);
        }
        if (value.lastName == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.lastName);
        }
        if (value.address == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.address);
        }
        if (value.phone == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.phone);
        }
        if (value.debt == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, value.debt);
        }
      }
    };
    this.__deletionAdapterOfClientEntity = new EntityDeletionOrUpdateAdapter<ClientEntity>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `clients` WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, ClientEntity value) {
        stmt.bindLong(1, value.id);
      }
    };
    this.__updateAdapterOfClientEntity = new EntityDeletionOrUpdateAdapter<ClientEntity>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR ABORT `clients` SET `id` = ?,`code` = ?,`first_name` = ?,`last_name` = ?,`address` = ?,`phone` = ?,`debt` = ? WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, ClientEntity value) {
        stmt.bindLong(1, value.id);
        if (value.code == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.code);
        }
        if (value.firstName == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.firstName);
        }
        if (value.lastName == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.lastName);
        }
        if (value.address == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.address);
        }
        if (value.phone == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.phone);
        }
        if (value.debt == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, value.debt);
        }
        stmt.bindLong(8, value.id);
      }
    };
  }

  @Override
  public void insert(final ClientEntity client) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfClientEntity.insert(client);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void delete(final ClientEntity client) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __deletionAdapterOfClientEntity.handle(client);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void update(final ClientEntity client) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __updateAdapterOfClientEntity.handle(client);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public List<ClientEntity> getAll() {
    final String _sql = "SELECT * FROM clients ORDER BY id";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfCode = CursorUtil.getColumnIndexOrThrow(_cursor, "code");
      final int _cursorIndexOfFirstName = CursorUtil.getColumnIndexOrThrow(_cursor, "first_name");
      final int _cursorIndexOfLastName = CursorUtil.getColumnIndexOrThrow(_cursor, "last_name");
      final int _cursorIndexOfAddress = CursorUtil.getColumnIndexOrThrow(_cursor, "address");
      final int _cursorIndexOfPhone = CursorUtil.getColumnIndexOrThrow(_cursor, "phone");
      final int _cursorIndexOfDebt = CursorUtil.getColumnIndexOrThrow(_cursor, "debt");
      final List<ClientEntity> _result = new ArrayList<ClientEntity>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final ClientEntity _item;
        _item = new ClientEntity();
        _item.id = _cursor.getLong(_cursorIndexOfId);
        if (_cursor.isNull(_cursorIndexOfCode)) {
          _item.code = null;
        } else {
          _item.code = _cursor.getString(_cursorIndexOfCode);
        }
        if (_cursor.isNull(_cursorIndexOfFirstName)) {
          _item.firstName = null;
        } else {
          _item.firstName = _cursor.getString(_cursorIndexOfFirstName);
        }
        if (_cursor.isNull(_cursorIndexOfLastName)) {
          _item.lastName = null;
        } else {
          _item.lastName = _cursor.getString(_cursorIndexOfLastName);
        }
        if (_cursor.isNull(_cursorIndexOfAddress)) {
          _item.address = null;
        } else {
          _item.address = _cursor.getString(_cursorIndexOfAddress);
        }
        if (_cursor.isNull(_cursorIndexOfPhone)) {
          _item.phone = null;
        } else {
          _item.phone = _cursor.getString(_cursorIndexOfPhone);
        }
        if (_cursor.isNull(_cursorIndexOfDebt)) {
          _item.debt = null;
        } else {
          _item.debt = _cursor.getString(_cursorIndexOfDebt);
        }
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public ClientEntity getById(final long id) {
    final String _sql = "SELECT * FROM clients WHERE id = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, id);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfCode = CursorUtil.getColumnIndexOrThrow(_cursor, "code");
      final int _cursorIndexOfFirstName = CursorUtil.getColumnIndexOrThrow(_cursor, "first_name");
      final int _cursorIndexOfLastName = CursorUtil.getColumnIndexOrThrow(_cursor, "last_name");
      final int _cursorIndexOfAddress = CursorUtil.getColumnIndexOrThrow(_cursor, "address");
      final int _cursorIndexOfPhone = CursorUtil.getColumnIndexOrThrow(_cursor, "phone");
      final int _cursorIndexOfDebt = CursorUtil.getColumnIndexOrThrow(_cursor, "debt");
      final ClientEntity _result;
      if(_cursor.moveToFirst()) {
        _result = new ClientEntity();
        _result.id = _cursor.getLong(_cursorIndexOfId);
        if (_cursor.isNull(_cursorIndexOfCode)) {
          _result.code = null;
        } else {
          _result.code = _cursor.getString(_cursorIndexOfCode);
        }
        if (_cursor.isNull(_cursorIndexOfFirstName)) {
          _result.firstName = null;
        } else {
          _result.firstName = _cursor.getString(_cursorIndexOfFirstName);
        }
        if (_cursor.isNull(_cursorIndexOfLastName)) {
          _result.lastName = null;
        } else {
          _result.lastName = _cursor.getString(_cursorIndexOfLastName);
        }
        if (_cursor.isNull(_cursorIndexOfAddress)) {
          _result.address = null;
        } else {
          _result.address = _cursor.getString(_cursorIndexOfAddress);
        }
        if (_cursor.isNull(_cursorIndexOfPhone)) {
          _result.phone = null;
        } else {
          _result.phone = _cursor.getString(_cursorIndexOfPhone);
        }
        if (_cursor.isNull(_cursorIndexOfDebt)) {
          _result.debt = null;
        } else {
          _result.debt = _cursor.getString(_cursorIndexOfDebt);
        }
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public ClientEntity getClientByCode(final String code) {
    final String _sql = "SELECT * FROM clients WHERE code = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (code == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, code);
    }
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfCode = CursorUtil.getColumnIndexOrThrow(_cursor, "code");
      final int _cursorIndexOfFirstName = CursorUtil.getColumnIndexOrThrow(_cursor, "first_name");
      final int _cursorIndexOfLastName = CursorUtil.getColumnIndexOrThrow(_cursor, "last_name");
      final int _cursorIndexOfAddress = CursorUtil.getColumnIndexOrThrow(_cursor, "address");
      final int _cursorIndexOfPhone = CursorUtil.getColumnIndexOrThrow(_cursor, "phone");
      final int _cursorIndexOfDebt = CursorUtil.getColumnIndexOrThrow(_cursor, "debt");
      final ClientEntity _result;
      if(_cursor.moveToFirst()) {
        _result = new ClientEntity();
        _result.id = _cursor.getLong(_cursorIndexOfId);
        if (_cursor.isNull(_cursorIndexOfCode)) {
          _result.code = null;
        } else {
          _result.code = _cursor.getString(_cursorIndexOfCode);
        }
        if (_cursor.isNull(_cursorIndexOfFirstName)) {
          _result.firstName = null;
        } else {
          _result.firstName = _cursor.getString(_cursorIndexOfFirstName);
        }
        if (_cursor.isNull(_cursorIndexOfLastName)) {
          _result.lastName = null;
        } else {
          _result.lastName = _cursor.getString(_cursorIndexOfLastName);
        }
        if (_cursor.isNull(_cursorIndexOfAddress)) {
          _result.address = null;
        } else {
          _result.address = _cursor.getString(_cursorIndexOfAddress);
        }
        if (_cursor.isNull(_cursorIndexOfPhone)) {
          _result.phone = null;
        } else {
          _result.phone = _cursor.getString(_cursorIndexOfPhone);
        }
        if (_cursor.isNull(_cursorIndexOfDebt)) {
          _result.debt = null;
        } else {
          _result.debt = _cursor.getString(_cursorIndexOfDebt);
        }
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  public static List<Class<?>> getRequiredConverters() {
    return Collections.emptyList();
  }
}
