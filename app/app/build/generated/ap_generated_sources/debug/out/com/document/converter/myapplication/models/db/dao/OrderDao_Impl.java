package com.document.converter.myapplication.models.db.dao;

import android.database.Cursor;
import androidx.room.EntityDeletionOrUpdateAdapter;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.document.converter.myapplication.models.db.entities.OrderEntity;
import java.lang.Class;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings({"unchecked", "deprecation"})
public final class OrderDao_Impl implements OrderDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<OrderEntity> __insertionAdapterOfOrderEntity;

  private final EntityDeletionOrUpdateAdapter<OrderEntity> __deletionAdapterOfOrderEntity;

  private final EntityDeletionOrUpdateAdapter<OrderEntity> __updateAdapterOfOrderEntity;

  public OrderDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfOrderEntity = new EntityInsertionAdapter<OrderEntity>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR ABORT INTO `orders` (`id`,`client_id`,`agent_id`,`number`,`date`,`state`) VALUES (nullif(?, 0),?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, OrderEntity value) {
        stmt.bindLong(1, value.id);
        stmt.bindLong(2, value.clientId);
        stmt.bindLong(3, value.agentId);
        if (value.number == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.number);
        }
        if (value.date == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.date);
        }
        if (value.state == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.state);
        }
      }
    };
    this.__deletionAdapterOfOrderEntity = new EntityDeletionOrUpdateAdapter<OrderEntity>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `orders` WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, OrderEntity value) {
        stmt.bindLong(1, value.id);
      }
    };
    this.__updateAdapterOfOrderEntity = new EntityDeletionOrUpdateAdapter<OrderEntity>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR ABORT `orders` SET `id` = ?,`client_id` = ?,`agent_id` = ?,`number` = ?,`date` = ?,`state` = ? WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, OrderEntity value) {
        stmt.bindLong(1, value.id);
        stmt.bindLong(2, value.clientId);
        stmt.bindLong(3, value.agentId);
        if (value.number == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.number);
        }
        if (value.date == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.date);
        }
        if (value.state == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.state);
        }
        stmt.bindLong(7, value.id);
      }
    };
  }

  @Override
  public void insert(final OrderEntity order) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfOrderEntity.insert(order);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void delete(final OrderEntity order) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __deletionAdapterOfOrderEntity.handle(order);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void update(final OrderEntity order) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __updateAdapterOfOrderEntity.handle(order);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public List<OrderEntity> getAll() {
    final String _sql = "SELECT * FROM orders";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfClientId = CursorUtil.getColumnIndexOrThrow(_cursor, "client_id");
      final int _cursorIndexOfAgentId = CursorUtil.getColumnIndexOrThrow(_cursor, "agent_id");
      final int _cursorIndexOfNumber = CursorUtil.getColumnIndexOrThrow(_cursor, "number");
      final int _cursorIndexOfDate = CursorUtil.getColumnIndexOrThrow(_cursor, "date");
      final int _cursorIndexOfState = CursorUtil.getColumnIndexOrThrow(_cursor, "state");
      final List<OrderEntity> _result = new ArrayList<OrderEntity>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final OrderEntity _item;
        _item = new OrderEntity();
        _item.id = _cursor.getLong(_cursorIndexOfId);
        _item.clientId = _cursor.getLong(_cursorIndexOfClientId);
        _item.agentId = _cursor.getLong(_cursorIndexOfAgentId);
        if (_cursor.isNull(_cursorIndexOfNumber)) {
          _item.number = null;
        } else {
          _item.number = _cursor.getString(_cursorIndexOfNumber);
        }
        if (_cursor.isNull(_cursorIndexOfDate)) {
          _item.date = null;
        } else {
          _item.date = _cursor.getString(_cursorIndexOfDate);
        }
        if (_cursor.isNull(_cursorIndexOfState)) {
          _item.state = null;
        } else {
          _item.state = _cursor.getString(_cursorIndexOfState);
        }
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<OrderEntity> getAllByAgentId(final long agentId) {
    final String _sql = "SELECT * FROM orders WHERE agent_id = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, agentId);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfClientId = CursorUtil.getColumnIndexOrThrow(_cursor, "client_id");
      final int _cursorIndexOfAgentId = CursorUtil.getColumnIndexOrThrow(_cursor, "agent_id");
      final int _cursorIndexOfNumber = CursorUtil.getColumnIndexOrThrow(_cursor, "number");
      final int _cursorIndexOfDate = CursorUtil.getColumnIndexOrThrow(_cursor, "date");
      final int _cursorIndexOfState = CursorUtil.getColumnIndexOrThrow(_cursor, "state");
      final List<OrderEntity> _result = new ArrayList<OrderEntity>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final OrderEntity _item;
        _item = new OrderEntity();
        _item.id = _cursor.getLong(_cursorIndexOfId);
        _item.clientId = _cursor.getLong(_cursorIndexOfClientId);
        _item.agentId = _cursor.getLong(_cursorIndexOfAgentId);
        if (_cursor.isNull(_cursorIndexOfNumber)) {
          _item.number = null;
        } else {
          _item.number = _cursor.getString(_cursorIndexOfNumber);
        }
        if (_cursor.isNull(_cursorIndexOfDate)) {
          _item.date = null;
        } else {
          _item.date = _cursor.getString(_cursorIndexOfDate);
        }
        if (_cursor.isNull(_cursorIndexOfState)) {
          _item.state = null;
        } else {
          _item.state = _cursor.getString(_cursorIndexOfState);
        }
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public OrderEntity getById(final long id) {
    final String _sql = "SELECT * FROM orders WHERE id = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, id);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfClientId = CursorUtil.getColumnIndexOrThrow(_cursor, "client_id");
      final int _cursorIndexOfAgentId = CursorUtil.getColumnIndexOrThrow(_cursor, "agent_id");
      final int _cursorIndexOfNumber = CursorUtil.getColumnIndexOrThrow(_cursor, "number");
      final int _cursorIndexOfDate = CursorUtil.getColumnIndexOrThrow(_cursor, "date");
      final int _cursorIndexOfState = CursorUtil.getColumnIndexOrThrow(_cursor, "state");
      final OrderEntity _result;
      if(_cursor.moveToFirst()) {
        _result = new OrderEntity();
        _result.id = _cursor.getLong(_cursorIndexOfId);
        _result.clientId = _cursor.getLong(_cursorIndexOfClientId);
        _result.agentId = _cursor.getLong(_cursorIndexOfAgentId);
        if (_cursor.isNull(_cursorIndexOfNumber)) {
          _result.number = null;
        } else {
          _result.number = _cursor.getString(_cursorIndexOfNumber);
        }
        if (_cursor.isNull(_cursorIndexOfDate)) {
          _result.date = null;
        } else {
          _result.date = _cursor.getString(_cursorIndexOfDate);
        }
        if (_cursor.isNull(_cursorIndexOfState)) {
          _result.state = null;
        } else {
          _result.state = _cursor.getString(_cursorIndexOfState);
        }
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public OrderEntity getByNumber(final String number) {
    final String _sql = "SELECT * FROM orders WHERE number = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (number == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, number);
    }
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfClientId = CursorUtil.getColumnIndexOrThrow(_cursor, "client_id");
      final int _cursorIndexOfAgentId = CursorUtil.getColumnIndexOrThrow(_cursor, "agent_id");
      final int _cursorIndexOfNumber = CursorUtil.getColumnIndexOrThrow(_cursor, "number");
      final int _cursorIndexOfDate = CursorUtil.getColumnIndexOrThrow(_cursor, "date");
      final int _cursorIndexOfState = CursorUtil.getColumnIndexOrThrow(_cursor, "state");
      final OrderEntity _result;
      if(_cursor.moveToFirst()) {
        _result = new OrderEntity();
        _result.id = _cursor.getLong(_cursorIndexOfId);
        _result.clientId = _cursor.getLong(_cursorIndexOfClientId);
        _result.agentId = _cursor.getLong(_cursorIndexOfAgentId);
        if (_cursor.isNull(_cursorIndexOfNumber)) {
          _result.number = null;
        } else {
          _result.number = _cursor.getString(_cursorIndexOfNumber);
        }
        if (_cursor.isNull(_cursorIndexOfDate)) {
          _result.date = null;
        } else {
          _result.date = _cursor.getString(_cursorIndexOfDate);
        }
        if (_cursor.isNull(_cursorIndexOfState)) {
          _result.state = null;
        } else {
          _result.state = _cursor.getString(_cursorIndexOfState);
        }
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  public static List<Class<?>> getRequiredConverters() {
    return Collections.emptyList();
  }
}
