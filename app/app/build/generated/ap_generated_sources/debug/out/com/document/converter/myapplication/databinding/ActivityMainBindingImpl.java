package com.document.converter.myapplication.databinding;
import com.document.converter.myapplication.R;
import com.document.converter.myapplication.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityMainBindingImpl extends ActivityMainBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.agent_info_field, 3);
        sViewsWithIds.put(R.id.agent_image, 4);
        sViewsWithIds.put(R.id.orders_field, 5);
        sViewsWithIds.put(R.id.text_order_main, 6);
        sViewsWithIds.put(R.id.text_order_count_main, 7);
        sViewsWithIds.put(R.id.button_orders_list, 8);
        sViewsWithIds.put(R.id.products_field, 9);
        sViewsWithIds.put(R.id.text_product_main, 10);
        sViewsWithIds.put(R.id.text_product_count_main, 11);
        sViewsWithIds.put(R.id.button_products_list, 12);
        sViewsWithIds.put(R.id.clients_field, 13);
        sViewsWithIds.put(R.id.text_client_main, 14);
        sViewsWithIds.put(R.id.text_client_count_main, 15);
        sViewsWithIds.put(R.id.button_clients_list, 16);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityMainBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 17, sIncludes, sViewsWithIds));
    }
    private ActivityMainBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.ImageView) bindings[4]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[3]
            , (android.widget.Button) bindings[16]
            , (android.widget.Button) bindings[8]
            , (android.widget.Button) bindings[12]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[13]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[5]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[9]
            , (android.widget.TextView) bindings[15]
            , (android.widget.TextView) bindings[14]
            , (android.widget.TextView) bindings[7]
            , (android.widget.TextView) bindings[6]
            , (android.widget.TextView) bindings[11]
            , (android.widget.TextView) bindings[10]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[1]
            );
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.tvAgentLastname.setTag(null);
        this.tvAgentName.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.agent == variableId) {
            setAgent((com.document.converter.myapplication.models.db.entities.AgentEntity) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setAgent(@Nullable com.document.converter.myapplication.models.db.entities.AgentEntity Agent) {
        this.mAgent = Agent;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.agent);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String agentLastName = null;
        com.document.converter.myapplication.models.db.entities.AgentEntity agent = mAgent;
        java.lang.String agentFirstName = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (agent != null) {
                    // read agent.lastName
                    agentLastName = agent.lastName;
                    // read agent.firstName
                    agentFirstName = agent.firstName;
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvAgentLastname, agentLastName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvAgentName, agentFirstName);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): agent
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}