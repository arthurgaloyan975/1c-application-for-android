package com.document.converter.myapplication.databinding;
import com.document.converter.myapplication.R;
import com.document.converter.myapplication.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityClientBindingImpl extends ActivityClientBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.text_client_page_title, 7);
        sViewsWithIds.put(R.id.text_page_client_first_name_const, 8);
        sViewsWithIds.put(R.id.text_page_client_last_name_const, 9);
        sViewsWithIds.put(R.id.text_client_page_code_const, 10);
        sViewsWithIds.put(R.id.text_client_page_address_const, 11);
        sViewsWithIds.put(R.id.text_client_page_phone_const, 12);
        sViewsWithIds.put(R.id.text_client_page_debt_const, 13);
        sViewsWithIds.put(R.id.text_client_edit_button, 14);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityClientBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 15, sIncludes, sViewsWithIds));
    }
    private ActivityClientBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.TextView) bindings[14]
            , (android.widget.TextView) bindings[4]
            , (android.widget.TextView) bindings[11]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[10]
            , (android.widget.TextView) bindings[6]
            , (android.widget.TextView) bindings[13]
            , (android.widget.TextView) bindings[5]
            , (android.widget.TextView) bindings[12]
            , (android.widget.TextView) bindings[7]
            , (android.widget.TextView) bindings[1]
            , (android.widget.TextView) bindings[8]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[9]
            );
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.textClientPageAddress.setTag(null);
        this.textClientPageCode.setTag(null);
        this.textClientPageDebt.setTag(null);
        this.textClientPagePhone.setTag(null);
        this.textPageClientFirstName.setTag(null);
        this.textPageClientLastName.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.client == variableId) {
            setClient((com.document.converter.myapplication.models.db.entities.ClientEntity) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setClient(@Nullable com.document.converter.myapplication.models.db.entities.ClientEntity Client) {
        this.mClient = Client;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.client);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String clientPhone = null;
        java.lang.String clientDebt = null;
        java.lang.String clientLastName = null;
        java.lang.String clientAddress = null;
        java.lang.String stringValueOfClientDebt = null;
        java.lang.String clientCode = null;
        java.lang.String stringValueOfClientCode = null;
        com.document.converter.myapplication.models.db.entities.ClientEntity client = mClient;
        java.lang.String clientFirstName = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (client != null) {
                    // read client.phone
                    clientPhone = client.phone;
                    // read client.debt
                    clientDebt = client.debt;
                    // read client.lastName
                    clientLastName = client.lastName;
                    // read client.address
                    clientAddress = client.address;
                    // read client.code
                    clientCode = client.code;
                    // read client.firstName
                    clientFirstName = client.firstName;
                }


                // read String.valueOf(client.debt)
                stringValueOfClientDebt = java.lang.String.valueOf(clientDebt);
                // read String.valueOf(client.code)
                stringValueOfClientCode = java.lang.String.valueOf(clientCode);
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textClientPageAddress, clientAddress);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textClientPageCode, stringValueOfClientCode);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textClientPageDebt, stringValueOfClientDebt);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textClientPagePhone, clientPhone);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textPageClientFirstName, clientFirstName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textPageClientLastName, clientLastName);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): client
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}