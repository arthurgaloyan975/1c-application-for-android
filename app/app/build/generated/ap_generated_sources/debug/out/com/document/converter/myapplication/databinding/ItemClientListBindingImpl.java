package com.document.converter.myapplication.databinding;
import com.document.converter.myapplication.R;
import com.document.converter.myapplication.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemClientListBindingImpl extends ItemClientListBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.linear_client_name, 5);
        sViewsWithIds.put(R.id.linear_client_code, 6);
        sViewsWithIds.put(R.id.text_client_code_const, 7);
        sViewsWithIds.put(R.id.linear_client_debt, 8);
        sViewsWithIds.put(R.id.text_client_debt_const, 9);
    }
    // views
    @NonNull
    private final android.widget.LinearLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemClientListBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 10, sIncludes, sViewsWithIds));
    }
    private ItemClientListBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.LinearLayout) bindings[6]
            , (android.widget.LinearLayout) bindings[8]
            , (android.widget.LinearLayout) bindings[5]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[7]
            , (android.widget.TextView) bindings[4]
            , (android.widget.TextView) bindings[9]
            , (android.widget.TextView) bindings[1]
            , (android.widget.TextView) bindings[2]
            );
        this.mboundView0 = (android.widget.LinearLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.textClientCode.setTag(null);
        this.textClientDebt.setTag(null);
        this.textClientFirstName.setTag(null);
        this.textClientLastName.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.client == variableId) {
            setClient((com.document.converter.myapplication.models.db.entities.ClientEntity) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setClient(@Nullable com.document.converter.myapplication.models.db.entities.ClientEntity Client) {
        this.mClient = Client;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.client);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String clientDebt = null;
        com.document.converter.myapplication.models.db.entities.ClientEntity client = mClient;
        java.lang.String clientLastName = null;
        java.lang.String clientFirstName = null;
        java.lang.String clientCode = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (client != null) {
                    // read client.debt
                    clientDebt = client.debt;
                    // read client.lastName
                    clientLastName = client.lastName;
                    // read client.firstName
                    clientFirstName = client.firstName;
                    // read client.code
                    clientCode = client.code;
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textClientCode, clientCode);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textClientDebt, clientDebt);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textClientFirstName, clientFirstName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textClientLastName, clientLastName);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): client
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}