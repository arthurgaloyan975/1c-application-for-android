package com.document.converter.myapplication.models.db.dao;

import android.database.Cursor;
import androidx.room.EntityDeletionOrUpdateAdapter;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.document.converter.myapplication.models.db.entities.AgentEntity;
import java.lang.Class;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings({"unchecked", "deprecation"})
public final class AgentDao_Impl implements AgentDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<AgentEntity> __insertionAdapterOfAgentEntity;

  private final EntityDeletionOrUpdateAdapter<AgentEntity> __deletionAdapterOfAgentEntity;

  private final EntityDeletionOrUpdateAdapter<AgentEntity> __updateAdapterOfAgentEntity;

  public AgentDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfAgentEntity = new EntityInsertionAdapter<AgentEntity>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR ABORT INTO `agents` (`id`,`first_name`,`last_name`,`login`,`password`) VALUES (nullif(?, 0),?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, AgentEntity value) {
        stmt.bindLong(1, value.id);
        if (value.firstName == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.firstName);
        }
        if (value.lastName == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.lastName);
        }
        if (value.login == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.login);
        }
        if (value.password == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.password);
        }
      }
    };
    this.__deletionAdapterOfAgentEntity = new EntityDeletionOrUpdateAdapter<AgentEntity>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `agents` WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, AgentEntity value) {
        stmt.bindLong(1, value.id);
      }
    };
    this.__updateAdapterOfAgentEntity = new EntityDeletionOrUpdateAdapter<AgentEntity>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR ABORT `agents` SET `id` = ?,`first_name` = ?,`last_name` = ?,`login` = ?,`password` = ? WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, AgentEntity value) {
        stmt.bindLong(1, value.id);
        if (value.firstName == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.firstName);
        }
        if (value.lastName == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.lastName);
        }
        if (value.login == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.login);
        }
        if (value.password == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.password);
        }
        stmt.bindLong(6, value.id);
      }
    };
  }

  @Override
  public void insert(final AgentEntity agent) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfAgentEntity.insert(agent);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void delete(final AgentEntity agent) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __deletionAdapterOfAgentEntity.handle(agent);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void update(final AgentEntity agent) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __updateAdapterOfAgentEntity.handle(agent);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public List<AgentEntity> getAll() {
    final String _sql = "SELECT * FROM agents";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfFirstName = CursorUtil.getColumnIndexOrThrow(_cursor, "first_name");
      final int _cursorIndexOfLastName = CursorUtil.getColumnIndexOrThrow(_cursor, "last_name");
      final int _cursorIndexOfLogin = CursorUtil.getColumnIndexOrThrow(_cursor, "login");
      final int _cursorIndexOfPassword = CursorUtil.getColumnIndexOrThrow(_cursor, "password");
      final List<AgentEntity> _result = new ArrayList<AgentEntity>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final AgentEntity _item;
        _item = new AgentEntity();
        _item.id = _cursor.getLong(_cursorIndexOfId);
        if (_cursor.isNull(_cursorIndexOfFirstName)) {
          _item.firstName = null;
        } else {
          _item.firstName = _cursor.getString(_cursorIndexOfFirstName);
        }
        if (_cursor.isNull(_cursorIndexOfLastName)) {
          _item.lastName = null;
        } else {
          _item.lastName = _cursor.getString(_cursorIndexOfLastName);
        }
        if (_cursor.isNull(_cursorIndexOfLogin)) {
          _item.login = null;
        } else {
          _item.login = _cursor.getString(_cursorIndexOfLogin);
        }
        if (_cursor.isNull(_cursorIndexOfPassword)) {
          _item.password = null;
        } else {
          _item.password = _cursor.getString(_cursorIndexOfPassword);
        }
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public AgentEntity getById(final long id) {
    final String _sql = "SELECT * FROM agents WHERE id = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, id);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfFirstName = CursorUtil.getColumnIndexOrThrow(_cursor, "first_name");
      final int _cursorIndexOfLastName = CursorUtil.getColumnIndexOrThrow(_cursor, "last_name");
      final int _cursorIndexOfLogin = CursorUtil.getColumnIndexOrThrow(_cursor, "login");
      final int _cursorIndexOfPassword = CursorUtil.getColumnIndexOrThrow(_cursor, "password");
      final AgentEntity _result;
      if(_cursor.moveToFirst()) {
        _result = new AgentEntity();
        _result.id = _cursor.getLong(_cursorIndexOfId);
        if (_cursor.isNull(_cursorIndexOfFirstName)) {
          _result.firstName = null;
        } else {
          _result.firstName = _cursor.getString(_cursorIndexOfFirstName);
        }
        if (_cursor.isNull(_cursorIndexOfLastName)) {
          _result.lastName = null;
        } else {
          _result.lastName = _cursor.getString(_cursorIndexOfLastName);
        }
        if (_cursor.isNull(_cursorIndexOfLogin)) {
          _result.login = null;
        } else {
          _result.login = _cursor.getString(_cursorIndexOfLogin);
        }
        if (_cursor.isNull(_cursorIndexOfPassword)) {
          _result.password = null;
        } else {
          _result.password = _cursor.getString(_cursorIndexOfPassword);
        }
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public AgentEntity getByLoginAndPassword(final String login, final String password) {
    final String _sql = "SELECT * FROM agents WHERE login = ? AND password = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 2);
    int _argIndex = 1;
    if (login == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, login);
    }
    _argIndex = 2;
    if (password == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, password);
    }
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfFirstName = CursorUtil.getColumnIndexOrThrow(_cursor, "first_name");
      final int _cursorIndexOfLastName = CursorUtil.getColumnIndexOrThrow(_cursor, "last_name");
      final int _cursorIndexOfLogin = CursorUtil.getColumnIndexOrThrow(_cursor, "login");
      final int _cursorIndexOfPassword = CursorUtil.getColumnIndexOrThrow(_cursor, "password");
      final AgentEntity _result;
      if(_cursor.moveToFirst()) {
        _result = new AgentEntity();
        _result.id = _cursor.getLong(_cursorIndexOfId);
        if (_cursor.isNull(_cursorIndexOfFirstName)) {
          _result.firstName = null;
        } else {
          _result.firstName = _cursor.getString(_cursorIndexOfFirstName);
        }
        if (_cursor.isNull(_cursorIndexOfLastName)) {
          _result.lastName = null;
        } else {
          _result.lastName = _cursor.getString(_cursorIndexOfLastName);
        }
        if (_cursor.isNull(_cursorIndexOfLogin)) {
          _result.login = null;
        } else {
          _result.login = _cursor.getString(_cursorIndexOfLogin);
        }
        if (_cursor.isNull(_cursorIndexOfPassword)) {
          _result.password = null;
        } else {
          _result.password = _cursor.getString(_cursorIndexOfPassword);
        }
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  public static List<Class<?>> getRequiredConverters() {
    return Collections.emptyList();
  }
}
