package com.document.converter.myapplication.databinding;
import com.document.converter.myapplication.R;
import com.document.converter.myapplication.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityOrderEditBindingImpl extends ActivityOrderEditBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.text_client_create_title, 5);
        sViewsWithIds.put(R.id.linear_order_edit_area, 6);
        sViewsWithIds.put(R.id.text_order_edit_number, 7);
        sViewsWithIds.put(R.id.text_order_edit_client, 8);
        sViewsWithIds.put(R.id.text_order_edit_date, 9);
        sViewsWithIds.put(R.id.text_order_edit_state, 10);
        sViewsWithIds.put(R.id.text_invalid_order_data, 11);
        sViewsWithIds.put(R.id.button_order_edit_apply, 12);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityOrderEditBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 13, sIncludes, sViewsWithIds));
    }
    private ActivityOrderEditBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.Button) bindings[12]
            , (android.widget.EditText) bindings[2]
            , (android.widget.EditText) bindings[3]
            , (android.widget.EditText) bindings[1]
            , (android.widget.EditText) bindings[4]
            , (android.widget.LinearLayout) bindings[6]
            , (android.widget.TextView) bindings[5]
            , (android.widget.TextView) bindings[11]
            , (android.widget.TextView) bindings[8]
            , (android.widget.TextView) bindings[9]
            , (android.widget.TextView) bindings[7]
            , (android.widget.TextView) bindings[10]
            );
        this.editOrderClient.setTag(null);
        this.editOrderDate.setTag(null);
        this.editOrderNumber.setTag(null);
        this.editOrderState.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.order == variableId) {
            setOrder((com.document.converter.myapplication.models.db.pojo.OrderWithClient) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setOrder(@Nullable com.document.converter.myapplication.models.db.pojo.OrderWithClient Order) {
        this.mOrder = Order;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.order);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String orderNumber = null;
        java.lang.String orderClientCode = null;
        java.lang.String orderDate = null;
        com.document.converter.myapplication.models.db.pojo.OrderWithClient order = mOrder;
        java.lang.String orderState = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (order != null) {
                    // read order.number
                    orderNumber = order.number;
                    // read order.clientCode
                    orderClientCode = order.clientCode;
                    // read order.date
                    orderDate = order.date;
                    // read order.state
                    orderState = order.state;
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.editOrderClient, orderClientCode);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.editOrderDate, orderDate);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.editOrderNumber, orderNumber);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.editOrderState, orderState);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): order
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}