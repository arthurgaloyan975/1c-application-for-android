package com.document.converter.myapplication.databinding;
import com.document.converter.myapplication.R;
import com.document.converter.myapplication.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityProductEditBindingImpl extends ActivityProductEditBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.text_product_edit_title, 6);
        sViewsWithIds.put(R.id.linear_product_create_area, 7);
        sViewsWithIds.put(R.id.text_product_edit_name, 8);
        sViewsWithIds.put(R.id.text_product_edit_code, 9);
        sViewsWithIds.put(R.id.text_product_edit_price, 10);
        sViewsWithIds.put(R.id.text_product_edit_count, 11);
        sViewsWithIds.put(R.id.text_product_edit_unit, 12);
        sViewsWithIds.put(R.id.button_product_edit_apply, 13);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityProductEditBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 14, sIncludes, sViewsWithIds));
    }
    private ActivityProductEditBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.Button) bindings[13]
            , (android.widget.EditText) bindings[2]
            , (android.widget.EditText) bindings[4]
            , (android.widget.EditText) bindings[1]
            , (android.widget.EditText) bindings[3]
            , (android.widget.EditText) bindings[5]
            , (android.widget.LinearLayout) bindings[7]
            , (android.widget.TextView) bindings[9]
            , (android.widget.TextView) bindings[11]
            , (android.widget.TextView) bindings[8]
            , (android.widget.TextView) bindings[10]
            , (android.widget.TextView) bindings[6]
            , (android.widget.TextView) bindings[12]
            );
        this.editProductCode.setTag(null);
        this.editProductCount.setTag(null);
        this.editProductName.setTag(null);
        this.editProductPrice.setTag(null);
        this.editProductUnit.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.product == variableId) {
            setProduct((com.document.converter.myapplication.models.db.entities.ProductEntity) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setProduct(@Nullable com.document.converter.myapplication.models.db.entities.ProductEntity Product) {
        this.mProduct = Product;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.product);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String stringValueOfProductCode = null;
        java.lang.String productCode = null;
        java.lang.String productName = null;
        com.document.converter.myapplication.models.db.entities.ProductEntity product = mProduct;
        java.lang.String productCount = null;
        java.lang.String productUnit = null;
        java.lang.String productPrice = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (product != null) {
                    // read product.code
                    productCode = product.code;
                    // read product.name
                    productName = product.name;
                    // read product.count
                    productCount = product.count;
                    // read product.unit
                    productUnit = product.unit;
                    // read product.price
                    productPrice = product.price;
                }


                // read String.valueOf(product.code)
                stringValueOfProductCode = java.lang.String.valueOf(productCode);
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.editProductCode, stringValueOfProductCode);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.editProductCount, productCount);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.editProductName, productName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.editProductPrice, productPrice);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.editProductUnit, productUnit);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): product
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}