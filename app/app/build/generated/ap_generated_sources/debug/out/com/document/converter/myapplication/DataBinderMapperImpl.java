package com.document.converter.myapplication;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.document.converter.myapplication.databinding.ActivityClientBindingImpl;
import com.document.converter.myapplication.databinding.ActivityClientEditBindingImpl;
import com.document.converter.myapplication.databinding.ActivityMainBindingImpl;
import com.document.converter.myapplication.databinding.ActivityOrderBindingImpl;
import com.document.converter.myapplication.databinding.ActivityOrderEditBindingImpl;
import com.document.converter.myapplication.databinding.ActivityProductBindingImpl;
import com.document.converter.myapplication.databinding.ActivityProductEditBindingImpl;
import com.document.converter.myapplication.databinding.ItemClientListBindingImpl;
import com.document.converter.myapplication.databinding.ItemOrderListBindingImpl;
import com.document.converter.myapplication.databinding.ItemProductListBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_ACTIVITYCLIENT = 1;

  private static final int LAYOUT_ACTIVITYCLIENTEDIT = 2;

  private static final int LAYOUT_ACTIVITYMAIN = 3;

  private static final int LAYOUT_ACTIVITYORDER = 4;

  private static final int LAYOUT_ACTIVITYORDEREDIT = 5;

  private static final int LAYOUT_ACTIVITYPRODUCT = 6;

  private static final int LAYOUT_ACTIVITYPRODUCTEDIT = 7;

  private static final int LAYOUT_ITEMCLIENTLIST = 8;

  private static final int LAYOUT_ITEMORDERLIST = 9;

  private static final int LAYOUT_ITEMPRODUCTLIST = 10;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(10);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.document.converter.myapplication.R.layout.activity_client, LAYOUT_ACTIVITYCLIENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.document.converter.myapplication.R.layout.activity_client_edit, LAYOUT_ACTIVITYCLIENTEDIT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.document.converter.myapplication.R.layout.activity_main, LAYOUT_ACTIVITYMAIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.document.converter.myapplication.R.layout.activity_order, LAYOUT_ACTIVITYORDER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.document.converter.myapplication.R.layout.activity_order_edit, LAYOUT_ACTIVITYORDEREDIT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.document.converter.myapplication.R.layout.activity_product, LAYOUT_ACTIVITYPRODUCT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.document.converter.myapplication.R.layout.activity_product_edit, LAYOUT_ACTIVITYPRODUCTEDIT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.document.converter.myapplication.R.layout.item_client_list, LAYOUT_ITEMCLIENTLIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.document.converter.myapplication.R.layout.item_order_list, LAYOUT_ITEMORDERLIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.document.converter.myapplication.R.layout.item_product_list, LAYOUT_ITEMPRODUCTLIST);
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case  LAYOUT_ACTIVITYCLIENT: {
          if ("layout/activity_client_0".equals(tag)) {
            return new ActivityClientBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_client is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYCLIENTEDIT: {
          if ("layout/activity_client_edit_0".equals(tag)) {
            return new ActivityClientEditBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_client_edit is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYMAIN: {
          if ("layout/activity_main_0".equals(tag)) {
            return new ActivityMainBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_main is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYORDER: {
          if ("layout/activity_order_0".equals(tag)) {
            return new ActivityOrderBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_order is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYORDEREDIT: {
          if ("layout/activity_order_edit_0".equals(tag)) {
            return new ActivityOrderEditBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_order_edit is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYPRODUCT: {
          if ("layout/activity_product_0".equals(tag)) {
            return new ActivityProductBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_product is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYPRODUCTEDIT: {
          if ("layout/activity_product_edit_0".equals(tag)) {
            return new ActivityProductEditBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_product_edit is invalid. Received: " + tag);
        }
        case  LAYOUT_ITEMCLIENTLIST: {
          if ("layout/item_client_list_0".equals(tag)) {
            return new ItemClientListBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for item_client_list is invalid. Received: " + tag);
        }
        case  LAYOUT_ITEMORDERLIST: {
          if ("layout/item_order_list_0".equals(tag)) {
            return new ItemOrderListBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for item_order_list is invalid. Received: " + tag);
        }
        case  LAYOUT_ITEMPRODUCTLIST: {
          if ("layout/item_product_list_0".equals(tag)) {
            return new ItemProductListBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for item_product_list is invalid. Received: " + tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(5);

    static {
      sKeys.put(0, "_all");
      sKeys.put(1, "agent");
      sKeys.put(2, "client");
      sKeys.put(3, "order");
      sKeys.put(4, "product");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(10);

    static {
      sKeys.put("layout/activity_client_0", com.document.converter.myapplication.R.layout.activity_client);
      sKeys.put("layout/activity_client_edit_0", com.document.converter.myapplication.R.layout.activity_client_edit);
      sKeys.put("layout/activity_main_0", com.document.converter.myapplication.R.layout.activity_main);
      sKeys.put("layout/activity_order_0", com.document.converter.myapplication.R.layout.activity_order);
      sKeys.put("layout/activity_order_edit_0", com.document.converter.myapplication.R.layout.activity_order_edit);
      sKeys.put("layout/activity_product_0", com.document.converter.myapplication.R.layout.activity_product);
      sKeys.put("layout/activity_product_edit_0", com.document.converter.myapplication.R.layout.activity_product_edit);
      sKeys.put("layout/item_client_list_0", com.document.converter.myapplication.R.layout.item_client_list);
      sKeys.put("layout/item_order_list_0", com.document.converter.myapplication.R.layout.item_order_list);
      sKeys.put("layout/item_product_list_0", com.document.converter.myapplication.R.layout.item_product_list);
    }
  }
}
