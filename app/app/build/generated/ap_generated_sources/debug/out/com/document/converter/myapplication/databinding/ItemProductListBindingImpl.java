package com.document.converter.myapplication.databinding;
import com.document.converter.myapplication.R;
import com.document.converter.myapplication.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemProductListBindingImpl extends ItemProductListBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.linear_product_code, 6);
        sViewsWithIds.put(R.id.text_product_code_const, 7);
        sViewsWithIds.put(R.id.linear_product_price, 8);
        sViewsWithIds.put(R.id.text_product_price_const, 9);
        sViewsWithIds.put(R.id.linear_product_count, 10);
        sViewsWithIds.put(R.id.text_product_count_const, 11);
    }
    // views
    @NonNull
    private final android.widget.LinearLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemProductListBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 12, sIncludes, sViewsWithIds));
    }
    private ItemProductListBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.LinearLayout) bindings[6]
            , (android.widget.LinearLayout) bindings[10]
            , (android.widget.LinearLayout) bindings[8]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[7]
            , (android.widget.TextView) bindings[4]
            , (android.widget.TextView) bindings[11]
            , (android.widget.TextView) bindings[1]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[9]
            , (android.widget.TextView) bindings[5]
            );
        this.mboundView0 = (android.widget.LinearLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.textProductCode.setTag(null);
        this.textProductCount.setTag(null);
        this.textProductName.setTag(null);
        this.textProductPrice.setTag(null);
        this.textProductUnit.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.product == variableId) {
            setProduct((com.document.converter.myapplication.models.db.entities.ProductEntity) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setProduct(@Nullable com.document.converter.myapplication.models.db.entities.ProductEntity Product) {
        this.mProduct = Product;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.product);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String stringValueOfProductCode = null;
        java.lang.String productCode = null;
        java.lang.String productName = null;
        com.document.converter.myapplication.models.db.entities.ProductEntity product = mProduct;
        java.lang.String productCount = null;
        java.lang.String productUnit = null;
        java.lang.String productPrice = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (product != null) {
                    // read product.code
                    productCode = product.code;
                    // read product.name
                    productName = product.name;
                    // read product.count
                    productCount = product.count;
                    // read product.unit
                    productUnit = product.unit;
                    // read product.price
                    productPrice = product.price;
                }


                // read String.valueOf(product.code)
                stringValueOfProductCode = java.lang.String.valueOf(productCode);
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textProductCode, stringValueOfProductCode);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textProductCount, productCount);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textProductName, productName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textProductPrice, productPrice);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textProductUnit, productUnit);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): product
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}