package com.document.converter.myapplication.models.db.dao;

import android.database.Cursor;
import androidx.room.EntityDeletionOrUpdateAdapter;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.document.converter.myapplication.models.db.entities.ClientsOrderEntity;
import java.lang.Class;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings({"unchecked", "deprecation"})
public final class ClientsOrderDao_Impl implements ClientsOrderDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<ClientsOrderEntity> __insertionAdapterOfClientsOrderEntity;

  private final EntityDeletionOrUpdateAdapter<ClientsOrderEntity> __deletionAdapterOfClientsOrderEntity;

  private final EntityDeletionOrUpdateAdapter<ClientsOrderEntity> __updateAdapterOfClientsOrderEntity;

  public ClientsOrderDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfClientsOrderEntity = new EntityInsertionAdapter<ClientsOrderEntity>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR ABORT INTO `clients_order` (`id`,`client_id`,`order_id`) VALUES (nullif(?, 0),?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, ClientsOrderEntity value) {
        stmt.bindLong(1, value.id);
        stmt.bindLong(2, value.clientId);
        stmt.bindLong(3, value.order_id);
      }
    };
    this.__deletionAdapterOfClientsOrderEntity = new EntityDeletionOrUpdateAdapter<ClientsOrderEntity>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `clients_order` WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, ClientsOrderEntity value) {
        stmt.bindLong(1, value.id);
      }
    };
    this.__updateAdapterOfClientsOrderEntity = new EntityDeletionOrUpdateAdapter<ClientsOrderEntity>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR ABORT `clients_order` SET `id` = ?,`client_id` = ?,`order_id` = ? WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, ClientsOrderEntity value) {
        stmt.bindLong(1, value.id);
        stmt.bindLong(2, value.clientId);
        stmt.bindLong(3, value.order_id);
        stmt.bindLong(4, value.id);
      }
    };
  }

  @Override
  public void insert(final ClientsOrderEntity clientsOrderEntity) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfClientsOrderEntity.insert(clientsOrderEntity);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void delete(final ClientsOrderEntity clientsOrderEntity) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __deletionAdapterOfClientsOrderEntity.handle(clientsOrderEntity);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void update(final ClientsOrderEntity clientsOrderEntity) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __updateAdapterOfClientsOrderEntity.handle(clientsOrderEntity);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public List<ClientsOrderEntity> getAll() {
    final String _sql = "SELECT * FROM clients_order";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfClientId = CursorUtil.getColumnIndexOrThrow(_cursor, "client_id");
      final int _cursorIndexOfOrderId = CursorUtil.getColumnIndexOrThrow(_cursor, "order_id");
      final List<ClientsOrderEntity> _result = new ArrayList<ClientsOrderEntity>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final ClientsOrderEntity _item;
        _item = new ClientsOrderEntity();
        _item.id = _cursor.getLong(_cursorIndexOfId);
        _item.clientId = _cursor.getLong(_cursorIndexOfClientId);
        _item.order_id = _cursor.getLong(_cursorIndexOfOrderId);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public ClientsOrderEntity getByOrderId(final long orderId) {
    final String _sql = "SELECT * FROM clients_order WHERE order_id = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, orderId);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfClientId = CursorUtil.getColumnIndexOrThrow(_cursor, "client_id");
      final int _cursorIndexOfOrderId = CursorUtil.getColumnIndexOrThrow(_cursor, "order_id");
      final ClientsOrderEntity _result;
      if(_cursor.moveToFirst()) {
        _result = new ClientsOrderEntity();
        _result.id = _cursor.getLong(_cursorIndexOfId);
        _result.clientId = _cursor.getLong(_cursorIndexOfClientId);
        _result.order_id = _cursor.getLong(_cursorIndexOfOrderId);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  public static List<Class<?>> getRequiredConverters() {
    return Collections.emptyList();
  }
}
