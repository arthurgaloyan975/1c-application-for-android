// Generated by data binding compiler. Do not edit!
package com.document.converter.myapplication.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.document.converter.myapplication.R;
import com.document.converter.myapplication.models.db.pojo.OrderWithClient;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class ActivityOrderBinding extends ViewDataBinding {
  @NonNull
  public final TextView textClientPageAddress;

  @NonNull
  public final TextView textClientPageCode;

  @NonNull
  public final TextView textOrderEditButton;

  @NonNull
  public final TextView textOrderPageClientConst;

  @NonNull
  public final TextView textOrderPageDateConst;

  @NonNull
  public final TextView textOrderPageNumberConst;

  @NonNull
  public final TextView textOrderPageState;

  @NonNull
  public final TextView textOrderPageStateConst;

  @NonNull
  public final TextView textOrderPageTitle;

  @NonNull
  public final TextView textPageClientFirstName;

  @Bindable
  protected OrderWithClient mOrder;

  protected ActivityOrderBinding(Object _bindingComponent, View _root, int _localFieldCount,
      TextView textClientPageAddress, TextView textClientPageCode, TextView textOrderEditButton,
      TextView textOrderPageClientConst, TextView textOrderPageDateConst,
      TextView textOrderPageNumberConst, TextView textOrderPageState,
      TextView textOrderPageStateConst, TextView textOrderPageTitle,
      TextView textPageClientFirstName) {
    super(_bindingComponent, _root, _localFieldCount);
    this.textClientPageAddress = textClientPageAddress;
    this.textClientPageCode = textClientPageCode;
    this.textOrderEditButton = textOrderEditButton;
    this.textOrderPageClientConst = textOrderPageClientConst;
    this.textOrderPageDateConst = textOrderPageDateConst;
    this.textOrderPageNumberConst = textOrderPageNumberConst;
    this.textOrderPageState = textOrderPageState;
    this.textOrderPageStateConst = textOrderPageStateConst;
    this.textOrderPageTitle = textOrderPageTitle;
    this.textPageClientFirstName = textPageClientFirstName;
  }

  public abstract void setOrder(@Nullable OrderWithClient order);

  @Nullable
  public OrderWithClient getOrder() {
    return mOrder;
  }

  @NonNull
  public static ActivityOrderBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.activity_order, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static ActivityOrderBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<ActivityOrderBinding>inflateInternal(inflater, R.layout.activity_order, root, attachToRoot, component);
  }

  @NonNull
  public static ActivityOrderBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.activity_order, null, false, component)
   */
  @NonNull
  @Deprecated
  public static ActivityOrderBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<ActivityOrderBinding>inflateInternal(inflater, R.layout.activity_order, null, false, component);
  }

  public static ActivityOrderBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static ActivityOrderBinding bind(@NonNull View view, @Nullable Object component) {
    return (ActivityOrderBinding)bind(component, view, R.layout.activity_order);
  }
}
