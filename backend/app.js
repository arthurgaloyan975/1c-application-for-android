const express = require("express");
const app = express();

app.use(express.json());

const database = require("./services/DatabaseService");
database.createTables();

const agentRouter = require("./routes/agentRouter");
const clientRouter = require("./routes/clientRouter");
const orderRouter = require("./routes/orderRouter");
const productRouter = require("./routes/productRouter");

app.use("/agent", agentRouter);
app.use("/client", clientRouter);
app.use("/order", orderRouter);
app.use("/product", productRouter);

app.listen(3000, function(err){
    if(err){
        console.log(err);
    }else{
        console.log("Server started on port 3000");
    }
});