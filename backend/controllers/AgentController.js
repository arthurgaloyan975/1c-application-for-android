const Agent = require("../models/Agent.js");

exports.create = (req, res) => {
    const reqData = req.body;
    const agent = new Agent({
        firstname: reqData.firstname,
        lastname: reqData.lastname,
        login: reqData.login,
        password: reqData.password
    });
    console.log(reqData.firstname);
    console.log(agent.firstname);

    Agent.create(agent, (err, data) => {
        if(err){
            res.status(500).send();   
        }else{
            res.send(data);
        }
    });
}

exports.getAll = (req, res) => {
    Agent.getAll((err, data) => {
        if(err){
            res.status(500).send();
        }else{
            res.send(data);
        }
    });
}

exports.getAgentByLoginPassword = (req, res) => {
    const data = {
        login: req.body.login,
        password: req.body.password
    };

    Agent.findByLoginPassword(data, (err, data) => {
        if(err){
            res.status(500).send(err);
        }
        else if(data){
            res.send(data);
        }else{
            res.send();
        }
    });

}

exports.update = (req, res) => {
    const id = req.params["id"];
    const reqData = req.body;
    const agent = new Agent({
        firstname: reqData.firstname,
        lastname: reqData.lastname,
        login: reqData.login,
        password: reqData.password
    })
    
    Agent.update(id, agent, (err, data) => {
        if(err){
            res.status(500).send(err);
        }else{
            res.status(200).send();
        }
    })
}

exports.delete = (req, res) => {
    const id = req.params["id"];
    Agent.delete(id, (err, data) => {
        if(err){
            res.status(500).send(err);
        }else{
            res.status(200).send();
        }
    });
}

exports.deleteAll = (req, res) => {
    Agent.deleteAll((err, data) => {
        if(err){
            res.status(500).send(err);
        }else{
            res.status(200).send();
        }
    });
}