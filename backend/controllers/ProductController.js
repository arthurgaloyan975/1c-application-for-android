const Product = require("../models/Product");

exports.create = (req, res) => {
    const reqData = req.body;
    const product = new Product({
        name: reqData.name,
        price: reqData.price,
        code: reqData.code,
        count: reqData.count,
        unit: reqData.unit
    });

    Product.create(product, (err, data) => {
        if(err){
            res.status(500).send();   
        }else{
            res.send("Done!");
        }
    });
}

exports.getAll = (req, res) => {
    Product.getAll((err, data) => {
        if(err){
            res.status(500).send();
        }else{
            res.send(data);
        }    
    });
}

exports.findById = (req, res) => {
    const id = req.params["id"];
    Product.getById(id, (err, data) => {
        if(err){
            res.status(500).send();
        }else{
            res.send(data);
        }
    });
}

exports.update = (req, res) => {
    const id = req.params["id"];
    const reqData = req.body;
    const product = new Product({
        name: reqData.name,
        price: reqData.price,
        code: reqData.code,
        count: reqData.count,
        unit: reqData.unit
    });
    
    Product.update(id, product, (err, data) => {
        if(err){
            res.status(500).send(err);
        }else{
            res.status(200).send();
        }
    })
}

exports.delete = (req, res) => {
    const id = req.params["id"];
    Product.delete(id, (err, data) => {
        if(err){
            res.status(500).send(err);
        }else{
            res.status(200).send();
        }
    });
}

exports.deleteAll = (req, res) => {
    Product.deleteAll((err, data) => {
        if(err){
            res.status(500).send(err);
        }else{
            res.status(200).send();
        }
    });
}