const Client = require("../models/Client.js");

exports.create = (req, res) => {
    const reqData = req.body;
    const client = new Client({
        firstname : reqData.firstname,
        lastname : reqData.lastname,
        code : reqData.code,
        phone : reqData.phone,
        address : reqData.address,
        debt : reqData.debt
    });

    Client.create(client, (err, data) =>{
        if(err){
            res.status(500).send();   
        }else{
            res.send("Done!");
        }
    });
}

exports.getAll = (req, res) => {
    Client.getAll((err, data) => {
        if(err){
            res.status(500).send();
        }else{
            res.send(data);
        }
    });
}

exports.findById = (req, res) => {
    const id = req.params["id"];
    Client.getById(id, (err, data) => {
        if(err){
            res.status(500).send();
        }else{
            res.send(data);
        }
    });
}

exports.update = (req, res) => {
    const id = req.params["id"];
    const reqData = req.body;
    const client = new Client({
        firstname : reqData.firstname,
        lastname : reqData.lastname,
        code : reqData.code,
        phone : reqData.phone,
        address : reqData.address,
        debt : reqData.debt
    });
    
    Client.update(id, client, (err, data) => {
        if(err){
            res.status(500).send(err);
        }else{
            res.status(200).send();
        }
    })
}

exports.delete = (req, res) => {
    const id = req.params["id"];
    Client.delete(id, (err, data) => {
        if(err){
            res.status(500).send(err);
        }else{
            res.status(200).send();
        }
    });
}

exports.deleteAll = (req, res) => {
    Client.deleteAll((err, data) => {
        if(err){
            res.status(500).send(err);
        }else{
            res.status(200).send();
        }
    });
}