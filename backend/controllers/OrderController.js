const Order = require("../models/Order");

exports.create = (req, res) => {
    const reqData = req.body;
    const order = new Order({
        number : reqData.number,
        date : reqData.date,
        state : reqData.state,
        clientCode : reqData.client_code
    });

    Order.create(order, (err, data) => {
        if(err){
            res.status(500).send();   
        }else{
            res.send("Done!");
        }
    });
}

exports.getAll = (req, res) => {
    Order.getAll((err, data) => {
        if(err){
            res.status(500).send();
        }else{
            res.send(data);
        }
    });
}

exports.findById = (req, res) => {
    const id = req.params["id"];
    Order.getById(id, (err, data) => {
        if(err){
            res.status(500).send();
        }else{
            res.send(data);
        }
    });
}

exports.findByAgent = (req, res) => {
    const AgentsOrder = require("../models/AgentsOrder");
    AgentsOrder.getByAgentId(req.params["agent_id"], (err, data) => {
        const orders = [];
        
        for(ordId in data){
            Order.findById(ordId.orderId, function(err, data){
                if(err){
                    res.status(500).send();
                }else{
                    orders.push(data);
                }
            });
        }
    });
}

exports.update = (req, res) => {
    const id = req.params["id"];
    const reqData = req.body;
    const order = new Order({
        number : reqData.number,
        date : reqData.date,
        state : reqData.state,
        clientCode : reqData.client_code
    });
    
    Order.update(id, order, (err, data) => {
        if(err){
            res.status(500).send(err);
        }else{
            res.status(200).send();
        }
    })
}

exports.delete = (req, res) => {
    const id = req.params["id"];
    Order.delete(id, (err, data) => {
        if(err){
            res.status(500).send(err);
        }else{
            res.status(200).send();
        }
    });
}

exports.deleteAll = (req, res) => {
    Order.deleteAll((err, data) => {
        if(err){
            res.status(500).send(err);
        }else{
            res.status(200).send();
        }
    });
}