const db = require("../services/DatabaseService");
const connection = db.connection;

const AgentsOrder = function(AgentsOrder){
    this.agentId = AgentsOrder.agentId;
    this.orderId = AgentsOrder.orderId;
}

AgentsOrder.add = (AgentsOrder, result) => {
    const insertQuery = "INSERT INTO agents_order(agent_id, order_id) VALUES (?, ?)";
    const data = [AgentsOrder.agentId, AgentsOrder.orderId];

    connection.execute(insertQuery, data, (err, res) => {
        if(err){
            console.log(err);
            result(err, null);
        }else{
            console.log(res);
            result(null, res);
        }
    });
}

AgentsOrder.getAll = (result) => {
    const selectQuery = "SELECT * FROM agents_order";

    connection.execute(selectQuery, (err, res) => {
        if(err){
            console.log(err);
            result(err, null);
        }else{
            console.log(res);
            result(null, res);
        }
    });
}

AgentsOrder.getByAgentId = (id, result) => {
    const selectQuery = "SELECT * FROM agents_order WHERE agent_id = ?";

    connection.execute(selectQuery, [id], (err, res) => {
        if(err){
            console.log(err);
            result(err, null);
        }else{
            console.log(res);
            result(null, res);
        }
    });
}

AgentsOrder.getByOrderId = (id, result) => {
    const selectQuery = "SELECT * FROM agents_order WHERE order_id = ?";

    connection.execute(selectQuery, [id], (err, res) => {
        if(err){
            console.log(err);
            result(err, null);
        }else{
            console.log(res);
            result(null, res);
        }
    });
}

AgentsOrder.update = (id, updated, result) => {
    const updateQuery = "UPDATE agents_order SET agent_id = ?, order_id = ? WHERE id = " + id;
    const data = [updated.agentId, updated.orderId];

    connection.execute(updateQuery, data, (err, res) => {
        if(err){
            console.log(err);
            result(err, null);
        }else{
            console.log(res);
            result(null, res);
        }
    });
}

AgentsOrder.delete = (id, result) => {
    const deleteQuery = "DELETE FROM agents_order WHERE id = ?";

    connection.execute(updateQuery, [id], (err, res) => {
        if(err){
            console.log(err);
            result(err, null);
        }else{
            console.log(res);
            result(null, null);
        }
    });
}

AgentsOrder.deleteAll = (result) => {
    const deleteQuery = "DELETE FROM agents_order";

    connection.execute(updateQuery, (err, res) => {
        if(err){
            console.log(err);
            result(err, null);
        }else{
            console.log(res);
            result(null, null);
        }
    });
}

module.exports = AgentsOrder;