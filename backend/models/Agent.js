const database = require("../services/DatabaseService");
const connection = database.connection;
        
connection.connect(function(err){
    if(err){
        console.log(err);
    }
});

const Agent = function(agent){
    this.firstname = agent.firstname;
    this.lastname = agent.lastname;
    this.login = agent.login;
    this.password = agent.password;
}


Agent.getAll = result => {
    const selectQuery = "SELECT * FROM agents";

    connection.execute(selectQuery, function(err, res){
        if(err){
            result(err, null);
        }else{
            result(null, res)
        }
    });

}

Agent.findByLoginPassword = (data, result) => {
    const auth = [data.login, data.password];
    const selectByLoginPasswordQuery = "SELECT * FROM agents WHERE login = ? AND password = ?";

    connection.execute(selectByLoginPasswordQuery, auth, function(err, res){
        if(err){
            result(err, null);
            return;
        }
        if (res.length){
            result(null, res);
            return;
        }

        result(null, null);
    });
}

Agent.create = (newAgent, result) => {
    const insertQuery = "INSERT INTO agents(firstName, lastName, login, password) VALUES (?, ?, ?, ?)";
    
    const data = [newAgent.firstname, newAgent.lastname, newAgent.login, newAgent.password];

    connection.execute(insertQuery, data, function(err, res){
        if(err){
            result(err, null);
        }{
            result(null, res);
        }
    });
}

Agent.update = (id, updatedAgent, result) => {
    const data = [updatedAgent.firstname, updatedAgent.lastname, updatedAgent.login, updatedAgent.password];
    const updateQuery = "UPDATE agents SET firstname = ?, lastname = ?, login = ?, password = ? WHERE id = " + id;

    connection.execute(updateQuery, data, function(err, res){
        if(err){
            result(err, null);
        }else{
            result(null, res);
        }
    });
}

Agent.delete = (id, result) => {
    const data = [id];
    const deleteQuery = "DELETE FROM agents WHERE id = ?";

    connection.execute(deleteQuery, data, function(err){
        if(err){
            result(err, null);
        }else{
            result(null, null);
        }
    });
}

Agent.deleteAll = (result) => {
    const deleteQuery = "DELETE FROM agents";

    connection.execute(deleteQuery, function(err){
        if(err){
            result(err, null);
        }else{
            result(null, null);
        }
    });
}

module.exports = Agent;