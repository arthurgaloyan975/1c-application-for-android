const database = require("../services/DatabaseService");
const connection = database.connection;

const Product = function(product){
    this.name = product.name;
    this.price = product.price;
    this.code = product.code;
    this.count = product.count;
    this.unit = product.unit;
}
    
Product.getAll = result => {
    const selectQuery = "SELECT * FROM products";

    connection.execute(selectQuery, function(err, res){
        if(err){
            result(ress, null);
        }else{
            result(null, res);
        }
    });
}

Product.getById = (id, result) => {
    const data = [id];
    const selectQuery = "SELECT * FROM products WHERE id = ?";

    connection.execute(selectQuery, data, function(err, res){
        if(err){
            result(ress, null);
        }else{
            result(null, res);
        }
    });
}

Product.create = (newProduct, result) => {
    const insertQuery = "INSERT INTO products(name, code, price, count, unit) VALUES (?, ?, ?, ?, ?)";
    const data = [newProduct.name, newProduct.code, newProduct.price, newProduct.count, newProduct.unit];

    connection.execute(insertQuery, data, function(err, res){
        if(err){
            result(err, null);
        }else{
            result(null, res)        
        }
    });
}

Product.update = (id, updatedProduct, result) => {
    const updateQuery = "UPDATE products SET name = ?, code = ?, price = ?, count = ?, unit = ? WHERE id = " + id;
    const data = [updatedProduct.name, updatedProduct.code, updatedProduct.price, updatedProduct.count, updatedProduct.unit];

    connection.execute(updateQuery, data, (err, res) => {
        if(err){
            result(err, null);
        }else{
            result(null, res)        
        }
    });
}

Product.delete = (id, result) => {
    const deleteQuery = "DELETE FROM products WHERE id = ?";

    connection.execute(deleteQuery, [id], function(err){
        if(err){
            result(err, null);
        }else{
            result(null, null);
        }
    });
}

Product.deleteAll = (result) => {
    const deleteQuery = "DELETE FROM products";

    connection.execute(deleteQuery, function(err){
        if(err){
            result(err, null);
        }else{
            result(null, null);
        }
    });
}

module.exports = Product;