const database = require("../services/DatabaseService");
const connection = database.connection;

const Order = function(order){
    this.number = order.number;
    this.date = order.date;
    this.state = order.state;
    this.clientCode = order.clientCode;
}

Order.getAll = result => {
    const selectQuery = "SELECT * FROM orders";

    connection.execute(selectQuery, function(err, res){
        if(err){
            result(ress, null);
        }else{
            result(null, res);
        }
    });
}

Order.getById = (id, result) => {
    const data = [id];
    const selectQuery = "SELECT * FROM orders WHERE id = ?";

    connection.execute(selectQuery, data, function(err, res){
        if(err){
            result(ress, null);
        }else{
            result(null, res);
        }
    });
}

Order.getByAgentId = (ids, result) => {
    const selectQuery = "SELECT * FROM orders WHERE id = ?";
}

Order.create = (newOrder, result) => {
    const insertQuery = "INSERT INTO orders(number, date, state, client_code) VALUES (?, ?, ?, ?)";
    const data = [newOrder.number, newOrder.date, newOrder.state, newOrder.clientCode];

    connection.execute(insertQuery, data, function(err, res){
        if(err){
            result(err, null);
        }else{
            result(null, res);        
        }
    });
}

Order.update = (id, updatedOrder, result) => {
    const updateQuery = "UPDATE orders SET number = ?, date = ?, state = ?, client_code = ? WHERE id = " + id;
    const data = [updatedOrder.number, updatedOrder.date, updatedOrder.state, updatedOrder.clientCode];

    connection.execute(updateQuery, data, (err, res) => {
        if(err){
            result(err, null);
        }else{
            result(null, res);        
        }
    });
}

Order.delete = (id, result) => {
    const deleteQuery = "DELETE FROM orders WHERE id = ?";

    connection.execute(deleteQuery, [id], function(err){
        if(err){
            result(err, null);
        }else{
            result(null, null);
        }
    });
}

Order.deleteAll = (result) => {
    const deleteQuery = "DELETE FROM orders";

    connection.execute(deleteQuery, function(err){
        if(err){
            result(err, null);
        }else{
            result(null, null);
        }
    });
}

module.exports = Order;
