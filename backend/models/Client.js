const database = require("../services/DatabaseService");
const connection = database.connection;

const Client = function(client){
    this.firstname = client.firstname;
    this.lastname = client.lastname;
    this.code = client.code;
    this.phone = client.phone;
    this.address = client.address;
    this.debt = client.debt;
}

Client.getAll = result => {
    const selectQuery = "SELECT * FROM clients";

    connection.execute(selectQuery, function(err, res){
        if(err){
            result(ress, null);
        }else{
            result(null, res);
        }
    });
}

Client.getById = (id, result) => {
    const selectQuery = "SELECT * FROM clients WHERE id = ?";

    connection.execute(selectQuery, [id], function(err, res){
        if(err){
            result(err, null);
        }else{
            result(null, res);
        }
    });
}

Client.create = (newClient, result) => {
    const insertQuery = "INSERT INTO clients(code, firstname, lastname, phone, address, debt) VALUES (?, ?, ?, ?, ?, ?)";
    const data = [newClient.code, newClient.firstname, newClient.lastname, newClient.phone, newClient.address, newClient.debt]

    connection.execute(insertQuery, data, function(err, res){
        if(err){
            result(err, null);
        }else{
            result(null, res)        
        }
    });
}

Client.update = (id, updatedClient, result) => {
    const updateQuery = "UPDATE clients SET firstname = ?, lastname = ?, code = ?, phone = ?, address = ?, debt = ? WHERE id = " + id;
    const data = [updatedClient.firstname, updatedClient.lastname, updatedClient.code, updatedClient.phone, updatedClient.address, updatedClient.debt];

    connection.execute(updateQuery, data, (err, res) => {
        if(err){
            result(err, null);
        }else{
            result(null, res)        
        }
    });
}

Client.delete = (id, result) => {
    const deleteQuery = "DELETE FROM clients WHERE id = ?";

    connection.execute(deleteQuery, [id], function(err){
        if(err){
            result(err, null);
        }else{
            result(null, null);
        }
    });
}

Client.deleteAll = (result) => {
    const deleteQuery = "DELETE FROM clients";

    connection.execute(deleteQuery, function(err){
        if(err){
            result(err, null);
        }else{
            result(null, null);
        }
    });
}

module.exports = Client;