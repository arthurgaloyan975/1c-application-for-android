const db = require("../services/DatabaseService");
const connection = db.connection;

const ClientsOrder = function(clientsOrder){
    this.clientId = clientsOrder.clientId;
    this.orderId = clientsOrder.orderId;
}

ClientsOrder.add = (clientsOrder, result) => {
    const insertQuery = "INSERT INTO clients_order(client_id, order_id) VALUES (?, ?)";
    const data = [clientsOrder.clientId, clientsOrder.orderId];

    connection.execute(insertQuery, data, (err, res) => {
        if(err){
            console.log(err);
            result(err, null);
        }else{
            console.log(res);
            result(null, res);
        }
    });
}

ClientsOrder.getAll = (result) => {
    const selectQuery = "SELECT * FROM clients_order";

    connection.execute(selectQuery, (err, res) => {
        if(err){
            console.log(err);
            result(err, null);
        }else{
            console.log(res);
            result(null, res);
        }
    });
}

ClientsOrder.getByClientId = (id, result) => {
    const selectQuery = "SELECT * FROM clients_order WHERE client_id = ?";

    connection.execute(selectQuery, [id], (err, res) => {
        if(err){
            console.log(err);
            result(err, null);
        }else{
            console.log(res);
            result(null, res);
        }
    });
}

ClientsOrder.getByOrderId = (id, result) => {
    const selectQuery = "SELECT * FROM clients_order WHERE order_id = ?";

    connection.execute(selectQuery, [id], (err, res) => {
        if(err){
            console.log(err);
            result(err, null);
        }else{
            console.log(res);
            result(null, res);
        }
    });
}

ClientsOrder.update = (id, updated, result) => {
    const updateQuery = "UPDATE clients_order SET client_id = ?, order_id = ? WHERE id = " + id;
    const data = [updated.clientId, updated.orderId];

    connection.execute(updateQuery, data, (err, res) => {
        if(err){
            console.log(err);
            result(err, null);
        }else{
            console.log(res);
            result(null, res);
        }
    });
}

ClientsOrder.delete = (id, result) => {
    const deleteQuery = "DELETE FROM clients_order WHERE id = ?";

    connection.execute(updateQuery, [id], (err, res) => {
        if(err){
            console.log(err);
            result(err, null);
        }else{
            console.log(res);
            result(null, null);
        }
    });
}

ClientsOrder.deleteAll = (result) => {
    const deleteQuery = "DELETE FROM clients_order";

    connection.execute(updateQuery, (err, res) => {
        if(err){
            console.log(err);
            result(err, null);
        }else{
            console.log(res);
            result(null, null);
        }
    });
}

module.exports = ClientsOrder;