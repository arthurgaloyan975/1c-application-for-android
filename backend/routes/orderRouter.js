const express = require("express");
const orderController = require("../controllers/OrderController");
const orderRoute = express.Router();

orderRoute.get("/", orderController.getAll);
orderRoute.post("/add", orderController.create);
orderRoute.get("/get/:id", orderController.findById);
//orderRoute.get("/get/agent/:id", orderController.findByAgent);
orderRoute.post("/update/:id", orderController.update);
orderRoute.get("/delete", orderController.deleteAll);
orderRoute.get("/delete/:id", orderController.delete);

module.exports = orderRoute;