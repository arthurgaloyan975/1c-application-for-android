const express = require("express");
const clientController = require("../controllers/ClientController");
const clientRoute = express.Router();

clientRoute.get("/", clientController.getAll);
clientRoute.post("/add", clientController.create);
clientRoute.get("/get/:id", clientController.findById);
clientRoute.post("/update/:id", clientController.update);
clientRoute.get("/delete", clientController.deleteAll);
clientRoute.get("/delete/:id", clientController.delete);

module.exports = clientRoute;