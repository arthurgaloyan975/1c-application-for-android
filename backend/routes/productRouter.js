const express = require("express");
const productRoute = express.Router();
const productController = require("../controllers/ProductController");

productRoute.get("/", productController.getAll);
productRoute.post("/add", productController.create);
productRoute.get("/get/:id", productController.findById);
productRoute.post("/update/:id", productController.update);
productRoute.get("/delete", productController.deleteAll);
productRoute.get("/delete/:id", productController.delete);

module.exports = productRoute;