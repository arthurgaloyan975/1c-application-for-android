const express = require("express");
const agentController = require("../controllers/AgentController");
const agentRouter = express.Router();

agentRouter.get("/", agentController.getAll);
agentRouter.get("/get", agentController.getAgentByLoginPassword);
agentRouter.post("/add", agentController.create);
agentRouter.post("/update/:id", agentController.update);
agentRouter.get("/delete/:id", agentController.delete);
agentRouter.get("/delete", agentController.deleteAll);

module.exports = agentRouter;