const mysql = require("mysql2");
const dbConfig = require("../config/db.config.js");

const connection = mysql.createConnection({
    host: dbConfig.HOST,
    user: dbConfig.USER,
    password: dbConfig.PASSWORD,
    database: dbConfig.DB
});

connection.connect(function(err){
    if(err){
        console.log(err);
    }
});

const close = () => {
    connection.end();
}

createAgentsTable = () => {
    const createAgentsTable = "CREATE TABLE IF NOT EXISTS agents(id INT NOT NULL AUTO_INCREMENT, firstname VARCHAR(25) NOT NULL, lastname VARCHAR(25) NOT NULL, login VARCHAR(25) NOT NULL, password VARCHAR(25) NOT NULL, created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP, updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, PRIMARY KEY(id))";
    connection.execute(createAgentsTable, function(err){
        if(err){console.log(err)}
    });
}

createClientsTable = () => {
    const createClientsTable = "CREATE TABLE IF NOT EXISTS clients(id INT NOT NULL AUTO_INCREMENT, firstname VARCHAR(25) NOT NULL, lastname VARCHAR(25) NOT NULL, code VARCHAR(25) NOT NULL, phone VARCHAR(25) NOT NULL, address VARCHAR(25) NOT NULL, debt VARCHAR(25),created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP, updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, PRIMARY KEY(id))";
    connection.execute(createClientsTable, function(err){
        if(err){console.log(err)}
    });
}

createOrdersTable = () => {
    const createOrdersTable = "CREATE TABLE IF NOT EXISTS orders(id INT NOT NULL AUTO_INCREMENT, number VARCHAR(25) NOT NULL, date VARCHAR(25) NOT NULL, state VARCHAR(25) NOT NULL, client_code VARCHAR(25) NOT NULL, created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP, updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, PRIMARY KEY(id))";
    connection.execute(createOrdersTable, function(err){
        if(err){console.log(err)}
    });
}

createProductsTable = () => {
    const createProductsTable = "CREATE TABLE IF NOT EXISTS products(id INT NOT NULL AUTO_INCREMENT, name VARCHAR(25) NOT NULL, code VARCHAR(25) NOT NULL, price INT(25) NOT NULL, count INT NOT NULL, unit VARCHAR(25) NOT NULL, created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP, updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, PRIMARY KEY(id))";
    connection.execute(createProductsTable, function(err){
        if(err){console.log(err)}
    });
}

const createTables = () => {
    createAgentsTable();
    createClientsTable();
    createOrdersTable();
    createProductsTable();
}

exports.connection = connection;
exports.createTables = createTables;
exports.close = close;